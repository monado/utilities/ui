#!/usr/bin/env python3
# Copyright 2020-2023, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0

import eel


####
# Hello world
#

@eel.expose
def say_hello_py(x):
    print('Hello from %s' % x)


def hello_world():
    say_hello_py('Python World!')
    eel.say_hello_js('Python World!')


####
# Main
#

def main():
    # Set web files folder
    eel.init('web')

    # Selects Chrome on Ubuntu 20.04, and in a normal tab.
    mode = 'chrome-app'
    # Selects Chromium on Ubuntu 20.04, in a app window.
    mode = 'chrome'
    # Automatically selects a port.
    port = 0

    eel.start('hello.html', mode=mode, size=(1280, 720), port=port)


def test():
    config = Config.load()
    config.save()


if __name__ == "__main__":
    main()
