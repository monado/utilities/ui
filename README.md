<!--
Copyright 2020-2023, Collabora, Ltd.
SPDX-License-Identifier: CC-BY-4.0
-->

# Monado setup UI

A config UI for Monado based on [eel](https://github.com/samuelhwilliams/Eel),
the web won, long live native UIs.
